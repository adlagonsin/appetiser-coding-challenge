//
//  MasterHeader+CoreDataProperties.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/16/20.
//  Copyright © 2020 adl. All rights reserved.
//
//

import Foundation
import CoreData


extension MasterHeader {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MasterHeader> {
        return NSFetchRequest<MasterHeader>(entityName: "MasterHeader")
    }

    @NSManaged public var albumArt: String?
    @NSManaged public var artistName: String?
    @NSManaged public var image: Data?
    @NSManaged public var lastOpened: String?
    @NSManaged public var trackId: Int32
    @NSManaged public var trackName: String?
    @NSManaged public var kind: String?
    @NSManaged public var detail: Detail?

}
