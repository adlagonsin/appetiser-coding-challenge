//
//  Tracks.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/13/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit

struct Tracks: Decodable {
    var resultCount:Int?
    var results:[Track]?
}

struct Track: Decodable {
    var trackId:Int?
    var trackName:String?
    var trackPrice:Double?
    var primaryGenreName:String?
    var artworkUrl100:String?
    var artistName:String?
    var longDescription:String?
    var previewUrl:String?
    var kind:String?
}
