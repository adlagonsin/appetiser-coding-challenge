//
//  Detail+CoreDataClass.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/14/20.
//  Copyright © 2020 adl. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Detail)
public class Detail: NSManagedObject {
    class func insert(context:NSManagedObjectContext) -> Detail {
        return Detail(entity: NSEntityDescription.entity(forEntityName: self.description(), in: context)!, insertInto: context)
    }
}
