//
//  Detail+CoreDataProperties.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/14/20.
//  Copyright © 2020 adl. All rights reserved.
//
//

import Foundation
import CoreData


extension Detail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Detail> {
        return NSFetchRequest<Detail>(entityName: "Detail")
    }

    @NSManaged public var isPurchased: Bool
    @NSManaged public var longDescription: String?
    @NSManaged public var previewUrl: String?
    @NSManaged public var price: Double
    @NSManaged public var genre: String?
    @NSManaged public var header: MasterHeader?

}
