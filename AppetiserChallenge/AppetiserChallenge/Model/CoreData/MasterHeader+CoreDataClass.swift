//
//  MasterHeader+CoreDataClass.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/14/20.
//  Copyright © 2020 adl. All rights reserved.
//
//

import Foundation
import CoreData

@objc(MasterHeader)
public class MasterHeader: NSManagedObject {
    class func insert(context:NSManagedObjectContext) -> MasterHeader {
        return MasterHeader(entity: NSEntityDescription.entity(forEntityName: self.description(), in: context)!, insertInto: context)
    }
}
