//
//  AppDelegate.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/13/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit
import CoreData
import FacebookCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        _ = DelegateManager.shared
        _ = CoreDataStack.shared
        
        
        //SDKApplicationDelegate.shared.application(application,didFinishLaunchingWithOptions: launchOptions)
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        return true
    }
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        return SDKApplicationDelegate.shared.application(application, open: url)
//    }
//
//    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
//    {
//        return SDKApplicationDelegate.shared.application(app, open: url, options: options)
//    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {

        if let window = window, let transitionViewClass = NSClassFromString("UITransitionView") {
            for transitionSubview in window.subviews where transitionSubview.isKind(of: transitionViewClass) {
                for subview in transitionSubview.subviews {
                    if
                        let avPlayerClass = NSClassFromString("AVFullScreenViewController"),
                        let nextResponder = subview.next,
                        nextResponder.isKind(of: avPlayerClass) {
                        return .portrait
                    }
                }
            }
        }

        return .portrait
    }

}

