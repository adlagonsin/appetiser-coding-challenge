//
//  Utilities.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/14/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit

class Utilities: NSObject {
    
    public typealias AlertCompletionHandler = (_ firstButton:Bool) -> ()
    
    class func alert(title:String, message:String, firstButtonTitle:String = "OK", secondButtonTitle:String? = nil,completion:AlertCompletionHandler? = nil) -> UIAlertController{
        
        //Initialize alert
        let alert = UIAlertController(title:title, message:message, preferredStyle:.alert)
        
        //Add actions
        let firstAction = UIAlertAction(title: firstButtonTitle, style: UIAlertAction.Style.default, handler:{ action in
            completion?(true)
        })
        alert.addAction(firstAction)
        
        if let title = secondButtonTitle{
            let secondAction = UIAlertAction(title: title, style: UIAlertAction.Style.cancel, handler:{ action in
                completion?(false)
            })
            alert.addAction(secondAction)
        }
        
        return alert
        
    }
}

extension UINavigationController {

    func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }

}

public extension UIView {
    func fadeIn(duration: TimeInterval = 1) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    func fadeOut(duration: TimeInterval = 1) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
}

extension UINavigationController {

    override open var shouldAutorotate: Bool {
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.shouldAutorotate
            }
            return super.shouldAutorotate
        }
    }

    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.preferredInterfaceOrientationForPresentation
            }
            return super.preferredInterfaceOrientationForPresentation
        }
    }

    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.supportedInterfaceOrientations
            }
            return super.supportedInterfaceOrientations
        }
    }
    
}

