//
//  UIDesignable.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/13/20.
//  Copyright © 2020 adl. All rights reserved.
//

import Foundation
import UIKit

protocol CorneredRadiusLayer {
  var cornerRadius: CGFloat { get set }
}

protocol BorderLayer {
  var borderColor: UIColor { get set }
  var borderWidth: CGFloat { get set }
}

@IBDesignable class UIDesignableUILabel:UILabel{
  @IBInspectable var borderWidth:CGFloat = 0.0 {
    didSet{
     self.layer.borderWidth = borderWidth
    }
  }
  
  @IBInspectable var borderColor: UIColor = UIColor.black {
    didSet {
      self.layer.borderColor = borderColor.cgColor
    }
  }
  @IBInspectable var cornerRadius: CGFloat = 0.0 {
    didSet {
      self.layer.cornerRadius = cornerRadius
    }
  }
}

// MARK: - ImageView
@IBDesignable class UIDesignableImageView: UIImageView, CorneredRadiusLayer, BorderLayer {
  @IBInspectable var cornerRadius: CGFloat = 0.0 {
    didSet {
      if cornerRadius > 0 {
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        
      }
    }
  }
  @IBInspectable var borderColor: UIColor = UIColor.black {
    didSet {
      layer.borderColor = borderColor.cgColor
      layer.masksToBounds = true
    }
  }
  @IBInspectable var borderWidth: CGFloat = 0.0 {
    didSet {
      layer.borderWidth = borderWidth
      layer.masksToBounds = true
    }
  }
}


// MARK: - View
@IBDesignable class UIDesignableView: UIView, CorneredRadiusLayer, BorderLayer {
  @IBInspectable var cornerRadius: CGFloat = 0.0 {
    didSet {
      if cornerRadius > 0 {
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        
      }
    }
  }
  @IBInspectable var borderColor: UIColor = UIColor.black {
    didSet {
      layer.borderColor = borderColor.cgColor
      layer.masksToBounds = true
    }
  }
  @IBInspectable var borderWidth: CGFloat = 0.0 {
    didSet {
      layer.borderWidth = borderWidth
      layer.masksToBounds = true
    }
  }
}

public extension UIView {
    
    func dropShadow(scale: Bool = true, opacity:Float = 0.8, radius:CGFloat = 2, offset:(width:CGFloat , height:CGFloat ) = (width:0,height:2), shadowRadius:CGFloat = 8) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = opacity
        
        layer.shadowOffset = CGSize(width: offset.width, height: offset.height)
        layer.shadowRadius = radius
        
        //layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: shadowRadius).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}




