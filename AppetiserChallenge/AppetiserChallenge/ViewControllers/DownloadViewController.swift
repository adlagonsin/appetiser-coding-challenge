//
//  DownloadViewController.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/13/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit
import CoreData

class DownloadViewController: BaseViewController {
    
    var headers:[MasterHeader]?

    @IBOutlet var downloadButton: UIButton!
    @IBOutlet var downloadButtonBg: UIDesignableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showLoader()
        
        //Check for existing entries
        let fetchQuery = NSFetchRequest<NSFetchRequestResult>(entityName: MasterHeader.description())
        
        do{
            if let results = try CoreDataStack.shared.context().fetch(fetchQuery) as? [MasterHeader]{
                headers = results
                
                //Show download button if no entries found
                if headers?.count != 0{
                    downloadButtonBg.isHidden = true
                } else {
                    // If last_screen is -1 then masterview is the last screen before app exit
                    UserManager.last_screen = -1
                    downloadButtonBg.isHidden = false
                }
            }
        } catch {
            
        }

        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
         //If entries found then proceed to track list
        if headers?.count != 0{
            self.performSegue(withIdentifier: "goToMasterView", sender: nil)
        }
        
        stopLoader()
    }
    
    @IBAction func downloadData(_ sender: Any) {
        
        showLoader()
        
        //Download data from API
        
        AFManager.shared.request(url: "https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all", method: .get, parameters: nil, headers: nil, structObject: Tracks.self) { (success, raw, results, error) in
            
            if success{
                
                for track in ((results as? Tracks)?.results)!{
                    
                    let header = MasterHeader.insert(context: CoreDataStack.shared.context())
                    
                    header.trackName = track.trackName
                    header.artistName = track.artistName
                    header.albumArt = track.artworkUrl100
                    header.kind = track.kind
                    
                    if let trackId = track.trackId{
                        header.trackId = Int32(trackId)
                    } else {
                        header.trackId = Int32.random(in: 1...1000000)
                    }
                    
                    header.lastOpened = ""
                    
                    let detail = Detail.insert(context: CoreDataStack.shared.context())
                    
                    detail.isPurchased = false
                    detail.longDescription = track.longDescription
                    detail.previewUrl = track.previewUrl
                    detail.price = track.trackPrice ?? 0.0
                    detail.genre = track.primaryGenreName
                    detail.header = header
                    
                    header.detail = detail
                    
                    self.headers?.append(header)
                    
                    CoreDataStack.shared.saveContext()
                    
                }
                
                self.stopLoader()
                
                self.performSegue(withIdentifier: "goToMasterView", sender: nil)
                
            } else {
                self.stopLoader()
            }
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        //Setup splitviewcontroller then pass headers to masterview
        
        if let splitViewController = segue.destination as? UISplitViewController {
            
            guard let navigationController = splitViewController.viewControllers.last as? UINavigationController else { return }
            navigationController.topViewController?.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
            navigationController.topViewController?.navigationItem.leftItemsSupplementBackButton = true
            splitViewController.delegate = segue.destination as? UISplitViewControllerDelegate

            let masterNavigationController = splitViewController.viewControllers[0] as! UINavigationController
            
            let controller = masterNavigationController.topViewController as! MasterViewController
        
            controller.headers = headers
            
            controller.managedObjectContext = CoreDataStack.shared.context()
            
        }
        
        
        
    }

}
