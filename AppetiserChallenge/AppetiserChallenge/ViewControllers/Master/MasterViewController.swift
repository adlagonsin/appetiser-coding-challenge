//
//  MasterViewController.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/13/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher
import AVKit

class MasterViewController: UITableViewController, DetailViewControllerDelegate {
    
    var detailViewController: DetailViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil
    
    //Composed of original headers without grouping
    ///filtered headers are used for search (used when isFiltering is true)
    var headers:[MasterHeader]?
    var filteredHeaders: [MasterHeader] = []
    
    //Default view of list upon initialization. Grouping is automatically implemented
    ///headerSections - names/letters of each section
    ///sortedHeaders - entries for each section are contained in a 2D array
    var headerSections:[String] = []
    var sortedHeaders:[[MasterHeader]] = []
    
    let searchController = UISearchController()
    var fetchedResultsController = NSFetchedResultsController<NSFetchRequestResult>()
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
    
    @IBOutlet var masterTableView: UITableView!
    
    var sortingMode = "trackName"


    override func viewDidLoad() {
        super.viewDidLoad()
        
        masterTableView.register(UINib(nibName: "MasterTableViewCell", bundle: nil), forCellReuseIdentifier: "masterTableViewCell")

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        //Nav bar setup
        navigationController?.setStatusBar(backgroundColor: UIColor.systemIndigo)
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.backgroundColor = UIColor.systemIndigo
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        navigationController?.navigationBar.barTintColor = UIColor.systemIndigo
        
        //Add a search bar
        searchController.searchBar.barTintColor = UIColor.white
        searchController.searchBar.placeholder = "Search Track Name / Artist"
        searchController.searchBar.searchTextField.backgroundColor = UIColor.white
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        navigationItem.searchController = searchController
        
        searchController.searchBar.delegate = self
        
        groupHeaders(mode:"trackName")
    
        // If last screen is not -1 then automatically present detail view upon previous app exit
        if UserManager.last_screen != -1{
            
            for i in 0...headerSections.count-1{
                let count = sortedHeaders[i].count
                
                for x in 0...count-1{
                    if sortedHeaders[i][x].trackId == UserManager.last_screen{
                        //call didselect to proceed to detail view
                        self.tableView(masterTableView, didSelectRowAt: IndexPath(row: x, section: i))
                        break
                    }
                }
            }
            
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sort By", style: .plain, target: self, action: #selector(showSortingOptions))
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    @objc func showSortingOptions(){
        let alert = UIAlertController(title: "Sort By", message: "", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Track Name", style: .default , handler:{ (UIAlertAction)in
            self.sortingMode = "trackName"
            self.groupHeaders(mode:self.sortingMode)
        }))

        alert.addAction(UIAlertAction(title: "Artist", style: .default , handler:{ (UIAlertAction)in
            self.sortingMode = "artistName"
            self.groupHeaders(mode:self.sortingMode)
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))

        self.present(alert, animated: true, completion: {
        })
    }
    
    func groupHeaders(mode:String){
        
        sortedHeaders.removeAll()
        headerSections.removeAll()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: MasterHeader.description())
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "trackId", ascending: true)]
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: mode, ascending: true)]
        
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStack.shared.context(), sectionNameKeyPath: nil, cacheName: nil)
        
        
        do {
            try fetchedResultsController.performFetch()
            
            let results = (fetchedResultsController.fetchedObjects as? [MasterHeader])!
        
            var tempHeaders:[MasterHeader] = []
            
            // Generate headerSections
            for header in results{
                
                var sortVar:String?
                if mode == "artistName"{
                    sortVar = header.artistName
                } else {
                    sortVar = header.trackName
                }
                
                if let param = sortVar{
                    let initialLetter = Array<Character>(param.uppercased())
                    
                    if !headerSections.contains(String(initialLetter[0])){
                        //Append new letter to section headers
                        headerSections.append(String(initialLetter[0]))
                    }
                } else {
                    if !headerSections.contains("~"){
                        //Append new letter to section headers
                        headerSections.append("~")
                    }
                    
                }
            }
            //sort section headers
            headerSections = headerSections.sorted { $0.lowercased() < $1.lowercased() }
            
            //Populate sortedHeaders 2D array with data from fetchRequest
            var sectionIndex = 0
            var nilHeaders:[MasterHeader] = []
            
            for header in results{
                
                var sortVar:String?
                if mode == "artistName"{
                    sortVar = header.artistName
                } else {
                    sortVar = header.trackName
                }
                
                if let param = sortVar{
                    let initialLetter = Array<Character>(param.uppercased())
                    
                    //Check if first letter corresponds to section header
                    if String(initialLetter[0]) == headerSections[sectionIndex]{
                        tempHeaders.append(header)
                    } else {
                        //Append batch of headers to section
                        sortedHeaders.append(tempHeaders)
                        
                        //Clear temp headers so we can attach a new section
                        tempHeaders.removeAll()
                        sectionIndex+=1
                        
                        //Add initial header for new section
                        tempHeaders.append(header)
                    }
                } else {
                    nilHeaders.append(header)
                }
            }
            
            //append final batch of headers
            sortedHeaders.append(tempHeaders)
            sortedHeaders.append(nilHeaders)
            
            headers = results
            
            tableView.reloadData()
        } catch {
            
        }
        
    }


    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
    
        //If tableView.indexPathForSelectedRow is present then that means selection came from masterview and not from UserManager.last_screen value
        
        if let indexPath = tableView.indexPathForSelectedRow {
            
            var header = MasterHeader()
            
            if isFiltering {
                header = filteredHeaders[indexPath.row]
            } else {
                //header = headers![indexPath.row]
                header = sortedHeaders[indexPath.section][indexPath.row]
            }
            
            controller.header = header
        } else {
            
            //Find indexPath of entry with trackId == UserManager.last_screen
            var index = 0
            for header in headers!{
                if header.trackId == UserManager.last_screen { break }
                index+=1
            }
            
            let object = headers![index]
            controller.header = object
        }
        
        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        controller.navigationItem.leftItemsSupplementBackButton = true
        
        DelegateManager.shared.detailViewControllerDelegate = self
        
        detailViewController = controller
        
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        if isFiltering {
            tableView.contentInset = UIEdgeInsets(top: -30, left: 0, bottom: 0, right: 0)
            return 1
        }
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        return headerSections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering {
            return filteredHeaders.count
        }
//
//        return headers!.count
        
        return sortedHeaders[section].count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: 50, height: 30))
        
        label.font = UIFont.boldSystemFont(ofSize: 17)
        
        if isFiltering {
            view.backgroundColor = UIColor.white
            label.text = ""
        } else {
            view.backgroundColor = UIColor.groupTableViewBackground
            label.text = headerSections[section]
        }
        
        
        view.addSubview(label)
        
        return view
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "masterTableViewCell", for: indexPath) as? MasterTableViewCell
        
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        
        var header = MasterHeader()
        
        if isFiltering {
            header = filteredHeaders[indexPath.row]
        } else {
            header = sortedHeaders[indexPath.section][indexPath.row]
            //header = headers![indexPath.row]
        }
        
        cell?.trackName.text = header.trackName
        cell?.trackArtist.text = header.artistName
        
        if let image = header.image{
            if !image.isEmpty{
                cell?.albumArt.image = UIImage(data:image)
            } else {
                cell?.albumArt.kf.setImage(with: URL(string:header.albumArt ?? ""), placeholder: UIImage(named: "square-placeholder"))
            }
        } else {
            cell?.albumArt.kf.setImage(with: URL(string:header.albumArt ?? ""), placeholder: UIImage(named: "square-placeholder"))
        }
        
        cell?.albumArt.dropShadow()
        
        if header.detail!.isPurchased{
            cell?.purchased.isHidden = false
        } else {
            cell?.purchased.isHidden = true
        }
        
        if header.lastOpened == "" {
            cell?.lastOpened.text = "Last Opened:\n\n"
        } else {
            cell?.lastOpened.text = "Last Opened:\n\(header.lastOpened ?? "")"
        }
        
        return cell!
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var header = MasterHeader()
        
        if isFiltering {
            header = filteredHeaders[indexPath.row]
        } else {
            //header = headers![indexPath.row]
            header = sortedHeaders[indexPath.section][indexPath.row]
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy\nhh:mm"
        header.lastOpened = dateFormatter.string(from: Date())
        
        CoreDataStack.shared.saveContext()
        
        //Set last screen to the selected entry so that it is presented when the app restores
        UserManager.last_screen = Int(header.trackId)
        
        //If tableView.indexPathForSelectedRow is present then that means selection came from masterview and not from UserManager.last_screen value. Else transition without animation using showLastDetail segue
        if let indexPath = tableView.indexPathForSelectedRow{
            self.performSegue(withIdentifier: "showDetail", sender: nil)
        } else {
            self.performSegue(withIdentifier: "showLastDetail", sender: nil)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            // handle data deletion
            
            self.present(Utilities.alert(title: "Delete Track", message: "Are you sure you want to delete this track?", firstButtonTitle: "Delete", secondButtonTitle: "Cancel") { (firstButtonTapped) in
                if firstButtonTapped{
                    
                    var willUpdate = false
                    
                    if self.isFiltering {
                        
                        //delete entry in sortedHeaders
                        
                        var headerCount = 0
                        for i in 0...self.headerSections.count-1{
                            let count = 0
                            
                            var index = 0
                            
                            for header in self.sortedHeaders[i]{
                                if header.trackId == self.filteredHeaders[indexPath.row].trackId {
                                    self.sortedHeaders[i].remove(at:index)
                                    break
                                }
                                index+=1
                            }
                        }
                        
                        //delete entry in headers
                        var index = 0
                        for header in self.headers!{
                            if header.trackId == self.filteredHeaders[indexPath.row].trackId {
                                self.headers?.remove(at: index)
                                break
                            }
                            index+=1
                        }

                        CoreDataStack.shared.context().delete(self.filteredHeaders[indexPath.row])
                        self.filteredHeaders.remove(at: indexPath.row)
                        
                        
                    } else {
                        willUpdate = self.sortedHeaders[indexPath.section].count == 1 ? true : false
                        CoreDataStack.shared.context().delete(self.sortedHeaders[indexPath.section][indexPath.row])
                        self.sortedHeaders[indexPath.section].remove(at: indexPath.row)
                    }
                    
                    CoreDataStack.shared.saveContext()

                    tableView.deleteRows(at: [indexPath], with: .fade)
                    
                    if willUpdate{
                        self.updateTable()
                    }
                    
                }
            }, animated: true, completion: nil)
            
            
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
/// Call for DetailViewControllerDelegate
    
    func updateTable() {
        if isFiltering{
            masterTableView.reloadData()
        } else {
            groupHeaders(mode: sortingMode)
        }
    }

}

// MARK: -Search Handling

extension MasterViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            if let indices = masterTableView.indexPathsForVisibleRows {
                for index in indices {
                    if index.row == 0 {
                        masterTableView.setContentOffset(CGPoint(x: 0, y: -130), animated: true)
                    }
                }
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        groupHeaders(mode: sortingMode)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredHeaders = headers!.filter { (header: MasterHeader) -> Bool in
            
            var containsArtistName = false
            if let artistName = header.artistName{
                containsArtistName = artistName.lowercased().contains(searchText.lowercased())
            }
            
            var containsTrackName = false
            if let trackName = header.trackName{
                containsTrackName = trackName.lowercased().contains(searchText.lowercased())
            }
            
            return containsTrackName || containsArtistName
        }
        
        tableView.reloadData()
    }
}

