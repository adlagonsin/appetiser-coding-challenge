//
//  MasterTableViewCell.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/13/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit

class MasterTableViewCell: UITableViewCell {
    
    
    @IBOutlet var albumArt: UIImageView!
    @IBOutlet var trackName: UILabel!
    @IBOutlet var trackArtist: UILabel!
    @IBOutlet var purchased: UILabel!
    @IBOutlet var lastOpened: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
