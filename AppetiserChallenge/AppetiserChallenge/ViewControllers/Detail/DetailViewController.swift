//
//  DetailViewController.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/13/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit
import AVKit
import WebKit

protocol DetailViewControllerDelegate {
    func updateTable()
}

class DetailViewController: UIViewController {

    
    @IBOutlet var videoBg: UIView!
    @IBOutlet var bgAlbumArt: UIImageView!
    @IBOutlet var albumArt: UIImageView!
    @IBOutlet var albumArtContainer: UIDesignableView!
    
    @IBOutlet var filmRoll: UIDesignableImageView!
    
    @IBOutlet var filmRollBtn: UIButton!
    
    @IBOutlet var trackName: UILabel!
    @IBOutlet var artistName: UILabel!
    @IBOutlet var genre: UILabel!
    @IBOutlet var price: UIButton!
    @IBOutlet var longDescription: UITextView!
    
    @IBOutlet var editGenreBtn: UIButton!
    @IBOutlet var editTrackNameBtn: UIButton!
    @IBOutlet var editArtistNameBtn: UIButton!
    @IBOutlet var editAlbumArtBtn: UIButton!
    
    @IBOutlet var upperHighlight: UIView!
    @IBOutlet var headerBackground: UIDesignableView!
    
    var track:Track?
    var header:MasterHeader?
    
    var isCurrentlyEditing:Bool = false
    var tempPreviewUrl:String = ""
    
    let imagePicker = UIImagePickerController()
    var tempImage:Data?
    
    let playerViewController = AVPlayerViewController()
    var videoIsPlaying = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DelegateManager.shared.detailViewControllerDelegate?.updateTable()
        
        navigationItem.title = header?.trackName
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        navigationController?.navigationBar.tintColor = UIColor.white
        
        configureView()
        imagePickerSetup()
        
        // Do setup for keyboard handling
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    
    func initBackgroundPreview(){
        if let videoURL = header?.detail?.previewUrl{
            
            if !(videoURL.contains("youtube") || videoURL.contains("youtu.be")) && videoURL.contains(".m4v"){
                    
               let player = AVPlayer(url: URL(fileURLWithPath: videoURL))
               player.isMuted = true
               playerViewController.player = player
           
               playerViewController.player?.currentItem?.preferredForwardBufferDuration = TimeInterval(5)
               playerViewController.player?.automaticallyWaitsToMinimizeStalling = true
               
               //Observe AVPlayer Status state
               //Add observer for AVPlayer status and AVPlayerItem status
               self.playerViewController.player?.addObserver(self, forKeyPath: #keyPath(AVPlayer.status), options: [.new, .initial], context: nil)
               self.playerViewController.player?.addObserver(self, forKeyPath: #keyPath(AVPlayer.currentItem.status), options:[.new, .initial], context: nil)
               self.playerViewController.player?.addObserver(self, forKeyPath: #keyPath(AVPlayer.timeControlStatus), options:[.new, .initial], context: nil)
                
               //Check if video has finished playing
               let center = NotificationCenter.default
               center.addObserver(self, selector: #selector(videoDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: self.playerViewController.player?.currentItem)
               
               
               playerViewController.view.frame = self.videoBg.bounds
               self.addChild(playerViewController)
               playerViewController.showsPlaybackControls = false
               self.videoBg.addSubview(playerViewController.view)
               
               playerViewController.videoGravity = .resizeAspectFill
               playerViewController.player?.play()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initBackgroundPreview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.prefersLargeTitles = true
        
        // Set masterview to last screen as view disappears
        UserManager.last_screen = -1
        DelegateManager.shared.detailViewControllerDelegate?.updateTable()
        
        //remove player to avoid erratic behavior/crashes
        playerViewController.player?.pause()
        
        playerViewController.player = nil
        playerViewController.view.removeFromSuperview()
        playerViewController.removeFromParent()
        
        //remove observers
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func videoDidFinishPlaying(_ notification: Notification) {
        //Rewind video after it finishes playing
        if let playerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: CMTime.zero, completionHandler: nil)
        }
        
        //Continue playing after 5 seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.playerViewController.player?.play()
        }
         
     }
     
    

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        
        if let player = object as? AVPlayer{
            
            if keyPath == #keyPath(AVPlayer.timeControlStatus) {
                let newStatus: AVPlayer.TimeControlStatus
                if let newStatusAsNumber = change?[NSKeyValueChangeKey.newKey] as? NSNumber {
                    newStatus = AVPlayer.TimeControlStatus(rawValue: newStatusAsNumber.intValue)!
                } else {
                    newStatus = .paused
                }
                
                //Determine if video is currently playing, paused or buffering/waiting
                if newStatus == .paused{
                    //print("player is paused")
                    playerViewController.view.alpha = 0
                    //playerViewController.view.fadeOut()
                }
                
                if newStatus == .waitingToPlayAtSpecifiedRate{
                    //print("player is waiting")
                    playerViewController.view.alpha = 0
                    //playerViewController.view.fadeOut()
                }
                
                if newStatus == .playing{
                    //print("player is playing")
                    playerViewController.view.fadeIn()
                }
            }

        }
    }
    

    
    // MARK: -Keyboard handling
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        longDescription.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func configureView() {
        
        self.isCurrentlyEditing = false
        
        navigationItem.title = header?.trackName
        
        editGenreBtn.isUserInteractionEnabled = false
        editTrackNameBtn.isUserInteractionEnabled = false
        editArtistNameBtn.isUserInteractionEnabled = false
        editAlbumArtBtn.isUserInteractionEnabled = false
        longDescription.isEditable = false
        
        let rightBarButton = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editFields))
        rightBarButton.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = rightBarButton
        
        // Album art image handling
        /// uses album art from provided URL
        /// uses new image if changed from editing
        if let image = self.header?.image{
            if !image.isEmpty{
                bgAlbumArt.image = UIImage(data: image)
                albumArt.image = UIImage(data: image)
            } else {
                bgAlbumArt.kf.setImage(with: URL(string:header?.albumArt ?? ""), placeholder: UIImage(named: "square-placeholder"))
                albumArt.kf.setImage(with: URL(string:header?.albumArt ?? ""), placeholder: UIImage(named: "square-placeholder"))
            }
            
        } else {
            bgAlbumArt.kf.setImage(with: URL(string:header?.albumArt ?? ""), placeholder: UIImage(named: "square-placeholder"))
            albumArt.kf.setImage(with: URL(string:header?.albumArt ?? ""), placeholder: UIImage(named: "square-placeholder"))
        }
        
        if header?.kind == "feature-movie"{
            self.filmRollBtn.setImage(UIImage(named:"filmroll2")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
            self.filmRollBtn.tintColor = UIColor.lightGray
        } else {
            self.filmRollBtn.isHidden = true
        }
        self.filmRollBtn.isHidden = true
        //albumArt.dropShadow()
        albumArtContainer.dropShadow()
        
        // fill in all fields
        trackName.text = header?.trackName
        artistName.text = header?.artistName
        
        if let genreName = header?.detail?.genre{
            genre.text =  genreName
        } else {
            genre.text = ""
        }
        
        if (header?.detail!.isPurchased)!{
            price.isUserInteractionEnabled = false
            price.backgroundColor = UIColor.systemGreen
            price.setTitle("Purchased", for: .normal)
        } else {
            price.setTitle("Buy now for: $\(header?.detail?.price ?? 0.0)", for: .normal)
        }
        
        longDescription.text = header?.detail?.longDescription
    }
    
    @objc func editFields(){
        isCurrentlyEditing = true
        
        editGenreBtn.isUserInteractionEnabled = true
        editTrackNameBtn.isUserInteractionEnabled = true
        editArtistNameBtn.isUserInteractionEnabled = true
        editAlbumArtBtn.isUserInteractionEnabled = true
        longDescription.isEditable = true
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveFields))
    }
    
    @objc func saveFields(){
        
        self.present(Utilities.alert(title: "Save Changes", message: "Are you sure you want to save changes?", firstButtonTitle: "Save", secondButtonTitle: "Discard", completion: { (firstButtonTapped) in
            
            if firstButtonTapped{
                
                self.header?.trackName = self.trackName.text
                self.header?.artistName = self.artistName.text
                self.header?.detail?.genre = self.genre.text
                self.header?.detail?.longDescription = self.longDescription.text
                
                if !(self.tempPreviewUrl.isEmpty){
                    self.header?.detail?.previewUrl = self.tempPreviewUrl
                }
                self.tempPreviewUrl = ""
                
                self.navigationItem.title = self.header?.trackName
                
                if let img = self.tempImage{
                    if !img.isEmpty{
                        self.header?.image = img
                    }
                }
                
                CoreDataStack.shared.saveContext()
                
                self.configureView()
                
            } else {
                self.configureView()
            }
            
        }), animated: true) { }
        
        
    }
    
    @IBAction func previewTrack(_ sender: Any) {
        
        if isCurrentlyEditing{
            
            self.present(changeField(title: "Change Preview URL", message: "Tip: You can use youtube links", placeholder: "Preview URL", field: nil), animated: true, completion: nil)
            
        } else {
            // Handle for youtube links using webview
            
            if let previewURL = header?.detail?.previewUrl{
                if previewURL.contains("youtube") || previewURL.contains("youtu.be"){
                                
                    var mywkwebview: WKWebView?
                    let mywkwebviewConfig = WKWebViewConfiguration()

                    mywkwebviewConfig.allowsInlineMediaPlayback = true
                    mywkwebview = WKWebView(frame: self.view.frame, configuration: mywkwebviewConfig)

                    let myURL = URL(string: (header?.detail?.previewUrl)!)
                    let youtubeRequest = URLRequest(url: myURL!)

                    mywkwebview?.load(youtubeRequest)

                    guard let webView = mywkwebview else { return }
                    
                    let webViewController:ViewControllerPannable = ViewControllerPannable()
                    
                    webViewController.view.addSubview(webView)
            
                    //webViewController.modalPresentationStyle = .overFullScreen
                    self.present(webViewController, animated: true, completion: nil)
                    
                } else {
                    // Otherwise, use provided URL for preview
                    
                    let previewVC = PreviewViewController()
                    previewVC.previewURL = header?.detail?.previewUrl
                    previewVC.lastScreen = Int(header!.trackId)
                    previewVC.modalPresentationStyle = .formSheet
                  
                    navigationController?.modalPresentationStyle = .formSheet
                    navigationController?.pushViewController(previewVC, animated: false)
                    
                }
            }
            
            
        }
        
        
    }
    
    @IBAction func purchaseTrack(_ sender: Any) {
        
        self.present(Utilities.alert(title: "Buy Track", message: "Purchase this track for $\((header?.detail?.price)!)?", firstButtonTitle: "Purchase", secondButtonTitle: "Cancel", completion: { (firstButtonTapped) in
            
            if firstButtonTapped {
                self.header?.detail?.isPurchased = true
                CoreDataStack.shared.saveContext()
                
                self.price.setTitle("Purchased", for: .normal)
                self.price.backgroundColor = UIColor.systemGreen

            }
            
        }), animated: true, completion: nil)
        
    }
    
    
    
    // MARK: -Methods for Editing Fields
    
    @IBAction func editTrackName(_ sender: Any) {
        self.present(changeField(title: "Change Track Name", message: "", placeholder: "Track Name", field: self.trackName), animated: true, completion: nil)
    }
    
    
    @IBAction func editArtistName(_ sender: Any) {
        self.present(changeField(title: "Change Artist Name", message: "", placeholder: "Artist Name", field: self.artistName), animated: true, completion: nil)
    }
    
    @IBAction func editGenre(_ sender: Any) {
        
        self.present(changeField(title: "Change Genre", message: "", placeholder: "Genre", field: self.genre), animated: true, completion: nil)
        
    }
    
    @IBAction func editAlbumArt(_ sender: Any) {
        
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func changeField(title:String, message:String, placeholder:String, field:UILabel?) -> UIAlertController{
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addTextField { (textField) in
            
            if let changedField = field{
                if changedField.text!.isEmpty{
                    textField.placeholder = placeholder
                } else {
                    textField.text = field!.text!
                }
            } else {
                textField.text = self.header?.detail?.previewUrl
            }
            
            
            
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            if let changedField = field{
                changedField.text = textField!.text
            } else {
                self.tempPreviewUrl = textField!.text!
            }
            

        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        return alert
        
    }
    
}

 // MARK: -Image Picker Setup and Handling

extension DetailViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerSetup() {
        
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        bgAlbumArt.image = image
        albumArt.image = image
        
        guard let imageData = image.jpegData(compressionQuality: 1) else {
            // handle failed conversion
            print("jpg error")
            return
        }
        
        tempImage = imageData
        
        
        dismiss(animated: true)

    }
    
  
}


