//
//  PreviewViewController.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/16/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit
import AVKit

class PreviewViewController: UIViewController {
    
    var previewURL:String?
    var isUnplayed = true
    var lastScreen:Int?
    
    var player = AVPlayer()
    let playerViewController = AVPlayerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //self.view.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor.black
        let videoURL = previewURL
        player = AVPlayer(url: URL(fileURLWithPath: videoURL!))
        
        //self.view.alpha = 0
        
        playerViewController.player = player
        
        self.modalPresentationStyle = .formSheet
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isUnplayed{
            present(playerViewController, animated: true) {
                self.player.play()
            }
            isUnplayed = false
        } else {
            
            UserManager.last_screen = lastScreen!
            
            playerViewController.player?.pause()
            playerViewController.player = nil
            
            //self.dismiss(animated: false, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
