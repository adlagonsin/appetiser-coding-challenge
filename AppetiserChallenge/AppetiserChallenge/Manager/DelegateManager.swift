//
//  DelegateManager.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/14/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit

class DelegateManager: NSObject {
    
    static let shared = DelegateManager()
    
    var detailViewControllerDelegate:DetailViewControllerDelegate?
    
}
