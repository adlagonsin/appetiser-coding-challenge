//
//  AFManager.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/13/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit
import Alamofire


public typealias AFManagerCompletionHandler   = (_ success:Bool, _ rawData:Any?, _ processedData:Any?, _ error:Error? ) -> ()

class AFManager: NSObject {
    
    private override init() {}
    static let shared = AFManager()
    
    fileprivate var AFcompletionHandler: AFManagerCompletionHandler?
    
    func request<Element: Decodable>(url:String, method:HTTPMethod, parameters:Parameters?, headers:HTTPHeaders?, structObject:Element.Type, completionHandler:@escaping AFManagerCompletionHandler){
        
        AFcompletionHandler = completionHandler
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers).validate().responseJSON{ response in
            switch response.result {
            case .success:
                guard let data = response.data else {return}
                
                //print("DATA:\(response.result.value ?? "")")
                
                do{
                    let results = try JSONDecoder().decode(structObject, from:data)
                    self.AFcompletionHandler?(true,data,results,nil)
                }catch{
                    self.AFcompletionHandler?(false,nil,nil,error)
                }
                
            case .failure(let error):
                self.AFcompletionHandler?(false,nil,nil,error)
            }
        }
    }

}
