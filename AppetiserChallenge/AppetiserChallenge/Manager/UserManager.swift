//
//  UserManager.swift
//  AppetiserChallenge
//
//  Created by Macbook Air on 1/14/20.
//  Copyright © 2020 adl. All rights reserved.
//

import UIKit

private struct Keys {
  static let last_screen = "last_screen"
}

class UserManager: NSObject {
    static var last_screen:Int{
      get{
        return UserDefaults.standard.integer(forKey: Keys.last_screen)
      }set(newValue){
        UserDefaults.standard.set(newValue, forKey: Keys.last_screen)
        UserDefaults.standard.synchronize()
      }
    }
}

  
  
