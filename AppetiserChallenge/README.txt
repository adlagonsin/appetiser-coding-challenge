
-------------------------README-----------------------

Persistence

1. I used core data for the app since it involves simple data saving/access
2. UserDefaults was also used to keep track of the last screen the user was on 

Architecture

1. I decided to use MVC for this app as I wanted to implement extra features that would sort of take extra time if I had chosen MVVM.

2. One common design pattern that was used all throughout the app was the Singleton design pattern (CoreDataStack, DelegateManager, AFManager). I used the DelegateManager class for handling callback functions. While it has minimal usage here, since the callback/delegation can be implemented in straightforward manner in the case of this app, its use becomes evident when handling callback functions of classes that are already deep within the heirarchy for example:

You need to execute a function on class A but you're currently in class E
(For example, this occurs when you have embedded elements in the UI i.e A TableView embedded in a CollectionView)

Class A -> Class B -> Class C -> Class D -> Class E

A singleton instance, in this scenario, which can set Class E as the delegate of Class A without going through classes B,C,D, would be helpful. The method call to the delegate would be executed directly on Class A and no callback/delegate functions would need to be implemented on classes B,C,D in order for Class E to reach Class A.

3. KVO design pattern was used to handle background video preview to manage the state of AVPlayer. It is used to check if the video is buffering or not. If video is buffering album art will be shown else, continue playing the preview.


FEATURES

Master View 
1. Sort By functionality (Track Name, Artist)
2. Search functionality (Track Name, Artist)
3. Delete entry by swipe
4. Shows Last opened state, Purchased state

Detail View
1. Preview in background screen (will change to album art if video is unable to play; loops continuously)
2. Edit fields (every field except Buy button)
3. Edit Image/Album art 
4. Edit preview URL (you can use youtube links - utilizes webview)
5. Shows last screen before app exit
 

